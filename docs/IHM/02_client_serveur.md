---
author: Mireille Coilhac
title: Prérequis client-serveur
---

## I. Fichier HTML

### 1. Votre micro site : 

Avec votre éditeur de texte, créer la page suivante : 

```html
<!DOCTYPE html>
<html>
  <head>
  <meta charset="utf-8" />
  <title>Mon premier onglet</title>
  </head>
  
  <body>
 Voici ma première page
  </body>
</html>
```

* Sauvegarder ce fichier dans votre dossier personnel sous le nom `monSite.html`.
* Double-cliquez sur ce fichier.

Vous devez obtenir quelque chose qui ressemble à ceci : 

![mon site](images/monSite.png){ width=40% }

???+ question "Où est interprété ce fichier ? Par quel programme ?"

    ??? success "Solution"

        Le fichier est interprété localement sur votre ordinateur par votre navigateur.


## 2. Vous voulez faire héberger votre site sur un serveur.

👉 Nous utiliserons le serveur UwAmp (Serveur Wamp Apache, mySQL, PHP, SQLite). Il existe bien d’autre possibilités : Wamp, Laragon,  IIS, nginx...

😊 Aujourd’hui le serveur se trouve aussi sur votre machine. Le fonctionnement est identique à celui d’un serveur hébergé à distance.

![icone UwAmp](images/icone_Uwamp.jpg){ width=8% }

👉 **Activer UwAmp**

Cliquer sur l’icône de UwAmp, puis sur Démarrer si nécessaire.

Vous devez obtenir ceci : 

![UwAmp](images/uwamp.jpg){ width=60% }

!!! danger "Bug possible"

    * Avec la configuration faite en classe, il ne faut **absolument pas** réduire la fenêtre UwAmp (avec l'icône _), car vous ne pourrez plus l'ouvrir. Vous serez alors obligé de vous déconnecter de votre compte réseau de l'établissement, puis de vous reconnecter.
    * Vous pouvez déplacer la fenêtre UwAmp sur un côte de votre écran, si elle vous gêne.


👉 Cliquer sur le bouton "Dossier www", puis ajouter un dossier à votre nom, par exemple le dossier Dupond

👉 Copier le fichier que vous venez de créer `monSite.html` dans ce dossier à votre nom.

👉 Double-cliquer sur ce fichier `monSite.html`.

???+ question "Où est interprété ce fichier ? Par quel programme ?"

    ??? success "Solution"

        Le fichier est interprété localement sur votre ordinateur par votre navigateur.


👉 Revenez sur UwAmp, et cliquer sur le bouton "Navigateur www"

Vous devez obtenir quelque chose qui ressemble à ceci : 

![UwAmp](images/dupond.jpg){ width=60% }

👉 Cliquer sur votre dossier, vous devez obtenir  quelque chose qui ressemble à ceci :

![UwAmp](images/index_serveur.jpg){ width=60% }

👉 Double-cliquer sur ce fichier `monSite.html`.

???+ question "Où est interprété ce fichier ? Par quel programme ?"

    ??? success "Solution"

    	Ce fichier est interprété par le serveur UwAmp.

## II.	Fichier PHP

### 1.	Création d’un fichier PHP

Ecrire le fichier PHP suivant avec un éditeur de texte (notepad++, ou Sublime Text…) et l’enregistrer sous le nom **affichages.php** sur votre session.

```php
<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8"/>
    <title> Affichages  </title>
 </head>
<body>
  <p> Ceci est un affichage écrit en HTML </p>

<?php
echo "Ceci est un affichage écrit en PHP";
?>
</body>
</html>
```

???+ question "Ouvrez votre fichier avec votre navigateur"

	Qu'obtenez-vous ?

    ??? success "Solution"

    	On voit le code du fichier.

    	Le navigateur ne sait pas interpréter un ficihier `.php`

???+ question "Ouvrez votre fichier dans le serveur"

	Procédez comme précédemment pour copier votre fichier sur le serveur, et le lire sur  le serveur. Qu'obtenez-vous ?

    ??? success "Solution"

    	![affichages](images/affichages.jpg){ width=40% }

## III. Bilan

!!! abstract "En bref"

    ![client serveur](images/client_serveur.png){ width=60% }

    (Image Frédéric Junier)

    Une architecture web est constituée de deux logiciels communiquant : le navigateur client qui effectue une requête pour disposer d'un document présent sur un serveur Web.

    La communication entre le client et le serveur se fait par le protocole HTTP.

    HTTP : HyperText Transfer Protocol


!!! abstract "php"

    ![client serveur php](images/client_serv_php_site.png){ width=40% }

    * **Etape 1** :	Le navigateur envoie l'adresse que l'utilisateur a saisie (demande d’une page html ou php).

    * **Etape 2** : Apache (le serveur web) cherche dans son arborescence (www) si le fichier existe, et si celui-ci porte une extension reconnue comme une application PHP. Si c'est le cas, Apache transmet ce fichier au parseur PHP.

    * **Etape 3** : PHP interprète le fichier, c'est-à-dire qu'il va analyser et exécuter le code PHP qui se trouve entre les balises :  
    `<? PHP` et `?>`

    * **Etape 4** : Le serveur web renvoie un fichier ne contenant plus de code PHP, donc le résultat sous forme de page HTML au navigateur qui l'interprète et l'affiche.

## IV. Crédits

Auteurs : K. Arbel, M.Coilhac, V-X. Jumel


    	

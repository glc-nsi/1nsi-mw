---
author: Fabrice Nativel, Gilles Lassus, Jean-Louis Thirot, Valérie Mousseaux et Mireille Coilhac
title: JavaScript
---

## I. Découverte

[Vidéo Lumni](https://www.lumni.fr/video/notions-de-web-et-d-interface-homme-machine){ .md-button target="_blank" rel="noopener" }

🌐 Vous pouvez télécharger le document correspondant ici : [Document Lumni](https://medias2ftv.akamaized.net/videosread/education/PDF/NSI_Web_support.pdf){ .md-button target="_blank" rel="noopener" }


L'objectif de ce cours n'est pas de faire de vous des codeurs de JavaScript experts, mais simplement de vous donner les clés qui vous permettront d'explorer ce langage.

!!! info "JavaScript"

	JavaScript est un langage de programmation coté client (tout se passe sur votre machine) à contrario de php qui lui est déporté coté serveur.

	C'est un langage qui va nous permettre de créer de l'interaction entre l'utilisateur et le client.


![client-serveur](images/dynclient.png){: .center}


Jusqu'à présent, la page web envoyée par le serveur est :

1. identique quel que soit le client.
2. statique après réception sur l'ordinateur du client.

Le JavaScript va venir régler le problème n°2 : il est possible de fabriquer une page sur laquelle le client va pouvoir agir **localement**, sans avoir à redemander une nouvelle page au serveur.

Inventé en 1995 par [Brendan Eich](https://fr.wikipedia.org/wiki/Brendan_Eich){:target="_blank"} pour le navigateur Netscape, le langage JavaScript s'est imposé comme la norme auprès de tous les navigateurs pour apporter de l'interactivité aux pages web.

### A vous de jouer 1

???+ question

    Dans votre éditeur de texte (Notepad++ ou dans Sublime Text ou autre), recopier le fichier suivant (enregistré par exemple sous bouton_change_fond.html):

    ```html
    <!DOCTYPE html>
	<html>
		<head>
			<title>Exemple changer fond</title>
			<meta charset="utf-8">
		</head>


		<body>
			<h1> Mon bouton pour changer le fond </h1>
			<input type="button" value="Changer de couleur" onclick="changeCouleur()" />
		</body>
	</html>
	```

	Comment exécuter ce programme ?

	Que se passe-t-il ?

    ??? success "Solution"

        * Comment exécuter ce programme ?  
		Il suffit de l'enregistrer en format html en l'appelant "n'importe_quel_nom.html"
		* Que se passe-t-il ?  
		Une erreur est détectée car la fonction `changeCouleur` n'est pas définie.

???+ question "Compléter"

	Compléter de la façon suivante, avec du code JavaScript, qui se trouve entre les balises `<script>` et `</ script>`

	```html
	<!DOCTYPE html>
	<html>
		<head>
			<title>Exemple changer fond </title>
			<meta charset="utf-8">
			<script>
				function changeCouleur() {
				document.body.style.backgroundColor="orange"
				}
			</script>
		</head>


		<body>
			<h1> Mon bouton pour changer le fond </h1>
			<input type="button" value="Changer de couleur" onclick="changeCouleur()" />
		</body>
	</html>
	```
	Que se passe-t-il ?

    ??? success "Solution"

    	Lorsque l'on exécute ce programme, et que l'on clique sur le bouton "Changer de couleur", le fond du site devient orange.

### A vous de jouer 2

Nous pouvons aussi écrire du code JavaScript dans un fichier séparé. C'est ce qui est fait très souvent.

!!! abstract "Exemple de couple ```html``` / ```javascript``` minimal"
    Notre fichier ```index.html``` fait référence, au sein d'une balise ```<script>```, à un fichier externe ```script.js``` qui contiendra notre code JavaScript.   

    Suivre ce lien  [jsfiddle.net](https://jsfiddle.net/){ .md-button target="_blank" rel="noopener" }, puis recopier dans la partie HTML le code HTML, et dans la partie JavaScript le code JavaScript donnés ci-dessous.

	Pour visualiser le rendu, cliquer sur Run.

	

    - fichier ```index.html``` : 
    ```html
    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <title>un peu d'action</title>
        <link href="style.css" rel="stylesheet" type="text/css" />
      </head>
      <body>
        <script src="script.js"></script>
        <p>
        <h2>Une page web extrêmement dynamique</h2>
        </p>
        <div>

            <label>Changez la couleur d'arrière-plan:</label>

            <button type="button" onclick="choix('yellow');">jaune</button>

            <button type="button" onclick="choix('green');">vert</button>

            <button type="button" onclick="choix('purple');">violet</button> 
        </div>
        <div>
          <p>
          En JavaScript, le nom de la couleur choisie est :
          </p>
          <p id="resultat"></p>
        </div>
      </body>
    </html>
    ```


    - fichier ```script.js``` :
    ```javascript
    function choix(color){
        document.body.style.background = color;
        document.getElementById("resultat").innerHTML=color;
    }
    ```


!!! info "Les boutons"


	- Au sein du bouton déclaré par la balise ```button```, l'attribut  ```onclick``` reçoit le nom d'une fonction déclarée à l'intérieur du fichier ```script.js```, ici la fonction ```choix()```.
	- Cette fonction nous permet de modifier à la fois l'aspect esthétique de la page (changement de la couleur de background) mais aussi le contenu de cette page, en faisant afficher le nom de la couleur.

	La puissance du JavaScript permet de réaliser aujourd'hui des interfaces utilisateurs très complexes au sein d'un navigateur, équivalentes à celles produites par des logiciels externes (pensez à Discord, par ex.). 


## II. ☝️ Où placer le code JavaScript ?

!!! info "A savoir"

	Le JavaScript peut se placer à trois endroits différents :

	* Dans l’élément head d’une page HTML ;
	* Dans l’élément body d’une page HTML ;
	* Dans un fichier portant l’extension .js séparé.

Nous avons vu au-dessus un code écrit dans un fichier séparé. Nous allons tester les deux autres possibilités.

Vous pouvez utiliser le site Suivre ce lien  [jsfiddle.net](https://jsfiddle.net/){ .md-button target="_blank" rel="noopener" }

???+ question "Tester"

	Recopier le code suivant et le tester.

	```html
	<!DOCTYPE html>
	<html>
	<head>
		<title>Un peu de JS</title>
		<meta charset="UTF-8">
		<script> alert("J'ai besoin d'aide !!");</script>
	</head>


	<body>	
		<h1>Où placer le code JavaScript ?</h1>	
		<p>
			Le JavaScript peut se placer à trois endroits différents :
			<ul>
				<li>Dans l’élément head d’une page HTML ;</li>
				<li>Dans l’élément body d’une page HTML ;</li>
				<li>Dans un fichier portant l’extension .js séparé </li>
			</ul>
		</p>
		    
	</body>
	</html>
	```

???+ question "Tester"

	Recopier le code suivant et le tester.

	```html
	<!DOCTYPE html>
	<html>
	<head>
		<title>Un peu de JS</title>
		<meta charset="UTF-8">		
	</head>


	<body>	
		<script> alert("J'ai besoin d'aide !!");</script>
		<h1>Où placer le code JavaScript ?</h1>	
		<p>
			Le JavaScript peut se placer à trois endroits différents :
			<ul>
				<li>Dans l’élément head d’une page HTML ;</li>
				<li>Dans l’élément body d’une page HTML ;</li>
				<li>Dans un fichier portant l’extension .js séparé </li>
			</ul>
		</p>
		    
	</body>
	</html>
	```


## III. Un exemple : la méthode `getElementById`

Nous avons déjà utilisé cette méthode. Nous allons l'étudier un peu plus précisément.

!!! info "Syntaxe"

	La méthode `getElementById` permet de récupérer les informations d'une balise identifiée par son id.
	
	informations à récupérer = document.getElementById("nom");


???+ question "Tester"

    Recopier le code suivant, l'enregistrer en fichier html, puis l'exécuter avec Mozilla Firefox

    ```html
	<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8">
			<script>
			function changeCouleur(nouvelleCouleur) {
				document.getElementById("paragraphe").style.color = nouvelleCouleur;
			}
			</script>
		</head>
		<body>
			<p id="paragraphe"> 
			informations à récupérer = document.getElementById("nom");
			</p>
			<p>
				<ul>
	    		<li>document représente la page HTML, plus précisément l'intérieur de la fenêtre du navigateur : 
				la zone d'affichage sans la barre d'adresse et les boutons de navigation.</li>
	    		<li>nom désigne la valeur de l'attribut id d'une balise unique située dans la page.</li>
	    		</ul>
			</p>
			<button onclick="changeCouleur('blue');">blue</button>
			<button onclick="changeCouleur('red');">red</button>
		</body>
	</html>
	```

	Appuyer sur la touche F12 de votre clavier, puis sélectionner l'onglet Inspecteur.

	Vous lisez le code correspondant à la page affichée.

	Observer la ligne `<p id="paragraphe">`

	Cliquer sur les boutons, et observer le code html. Que se passe-t-il ?
	Rafraichissez la page. Que se passe-t-il ?

    ??? success "Solution"

        * Lorsque l'on clique sur le bouton "blue", le paragraphe devient écrit en bleu, et la ligne de code html a été modifiée :  
        `<p id="paragraphe" style="color: blue;">`
        * Lorsque l'on clique sur le bouton "red", le paragraphe devient écrit en rouge, et la ligne de code html a été modifiée :  
        `<p id="paragraphe" style="color: red;">`
        * Lorsque l'on rafraichit la page, elle redevient à son état initial, et la ligne de code html redevient :
        `<p id="paragraphe">`


!!! info "Syntaxe"

	document.getElementById('exemple').innerHTML = "Code HTML à écrire";

	La propriété innerHTML permet de remplacer complètement le contenu d’une balise identifiée par son attribut id. Pour faire simple, elle permet d’écrire à un endroit précis de la page, sans tout effacer.


???+ question "Tester"

    Recopier le code suivant, l'enregistrer en fichier html, puis l'exécuter avec Mozilla Firefox.

    ```html
    <!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<p id="exemple">Cliquez sur le bouton et ce texte changera.</p>
		<input type="button" value="clic" onclick="changetexte()">
		<script>
		function changetexte() {
			document.getElementById("exemple").innerHTML = "Texte changé !";
		}
		</script>
	</body>
	</html>
	```

	Appuyer sur la touche F12 de votre clavier, puis sélectionner l'onglet Inspecteur.

	Oberver la ligne `<p id="exemple">Cliquez sur le bouton et ce texte changera.</p>`

	Cliquer sur le bouton, et observer le code html. Que se passe-t-il ?
	Rafraichissez la page. Que se passe-t-il ?

	??? success "Solution"

		Lorsque l'on clique sur le bouton, la ligne :  
		`<p id="exemple">Cliquez sur le bouton et ce texte changera.</p>`  
		est remplacée par la ligne :  
		`<p id="exemple">Texte changé !</p>`  


???+ question "Tester"

	```html
	<!DOCTYPE html>
	<html>
		<head>
			<title>JavaScript</title>
			<meta charset="utf-8">
			<script>
				function AfficheDate()
				{
					document.getElementById("date").innerHTML = Date();
				}
				function Produit(a, b)
				{
					var p = a*b;
					document.getElementById("resultat").innerHTML = p;

				}
			</script>
		</head>
		<body>
			<p id="date">Date ?</p>
			<button type="button" onclick="AfficheDate()">Cliquer pour avoir la date</button>
			<p>3*4= <span id="resultat">???</span></p>
			<button type="button" onclick="Produit(3, 4)">Cliquer pour avoir le résultat</button>		
		</body>
	</html>
	```

	Appuyer sur la touche F12 de votre clavier, puis sélectionner l'onglet Inspecteur. Observez bien les modifications du code html


!!! abstract "Résumé"

    * JavaScript s'exécute côté client
    * JavaScript modifie le code html qui est exécuté par le navigateur pour afficher la page.


## IV. Exercice

🌵 Ecrivez votre propre petite page web, avec un peu de code JavaScript ...


## V. Crédits

Auteurs : Fabrice Nativel, Gilles Lassus, Jean-Louis Thirot, Valérie Mousseaux, Mireille Coilhac


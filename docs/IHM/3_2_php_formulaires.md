---
author: Mireille Coilhac
title: Les formulaires - Méthodes POST et GET
---

## I. Introduction

<center>
<iframe width="635" height="357" src="https://www.youtube.com/embed/XZ_sarpdgOE"  frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>

## II. Les formulaires

### 1. Qu’est-ce qu’un formulaire ?

Un formulaire créé une interface homme-machine (IHM) permettant une interaction avec l'utilisateur

![formulaire](images/formulaire.png){ width=40%; : .center }

### 2. Créer un formulaire simple

![prenom](images/form_prenom.jpg){ width=20%; align=right }
Nous allons créer le formulaire suivant : 

Une fois exécuté par Alice, elle a obtenu l’affichage suivant :

![](images/alice.jpg){ width=30% }
![](images/bonjour_alice.jpg){ width=30% }

???+ "Créer le fichier `formulaire_Bonjour.html` suivant"

    ```html
	<!DOCTYPE html>
	<html>
	 <head>
	  <meta charset="utf-8"/>
	    <title> Hello </title>
	 </head>
	<body>
	  <form action="bonjour.php" method="post">
	  	Entrez votre prénom : <br>
	  	<input type="text" name="prenom"> <br>
	  	<input type="submit" value="Envoyer">
	  </form>
	</body>
	</html>
	```

???+ question "Pour créer l'intéraction"

    L'attribut `action` du fichier `formulaire_Bonjour.html` précédant, donne l'url du programme PHP destiné à traiter les données validées. 

	Créer le fichier `bonjour.php` suivant 

	```html
	<!DOCTYPE html>
	<html>
	<head>
		<title>Hello</title>
	</head>
	<body>
		<?php
			$prenom = $_POST["prenom"];
			echo "Bonjour ".$prenom;
		?>
	</body>
	</html>
	```

???+ question "Utiliser le serveur UwAmp"

	* Recopier ces deux fichiers dans votre répertoire (du style C:\UwAmp\www\Dupond)
	* Exécuter le fichier formulaire_Bonjour.html

!!! info "Mon info"

    `$_POST` est un tableau associatif. Il contient tous les couples clé (les variables) / valeur transmis par la méthode `post`.
	

???+ question "Modification du formulaire "

	Reprendre l’exemple précédent, en rajoutant la saisie de l’âge.
    
    Vous enregistrerez les fichiers sous les noms `formulaire_bonjour_age.html` et `bonjour_age.php`

	Syntaxe pour saisir un nombre :
	`input type="number"`

	Votre programme devra afficher par exemple après exécution :  
	  
	Bonjour Alice  
	Vous avez 17 ans  

	??? success "Solution"

		```html
		<!DOCTYPE html>
		<html>
		<head>
		<meta charset="utf-8"/>
			<title> Hello </title>
		</head>
		<body>
		<form action="bonjour_age.php" method="post">
			Entrez votre prénom : <br>
			<input type="text" name="prenom"> <br><br>
			Entrez votre âge : <br>
			<input type="number" name="age"> <br>
			<input type="submit" value="Envoyer">
		</form>
		</body>
		</html>
		```

		et

		```html
		<!DOCTYPE html>
		<html>
		<head>
			<title>Hello</title>
		</head>
		<body>
			<?php
				$prenom = $_POST["prenom"];
				$age = $_POST["age"];
				echo "Bonjour " . $prenom . "<br>";
				echo "Vous avez " . $age . " ans"."<br>";
			?>
		</body>
		</html>
		```

## III. Méthode post et méthode get

!!! info "Mon info"

	Il existe deux méthodes pour récupérer les variables : la méthode **post** et la méthode **get**.

	La méthode **post** crée le tableau associatif $_POST, et la méthode **get** crée le tableau associatif $_GET.

### 1. Méthode get

???+ question "Tester get"

    Reprendre les fichiers formulaire_bonjour_age.html et bonjour_age.php et les enregistrer respectivement sous formulaire_bonjour_age_get.html et bonjour_age_get.php

	* Dans formulaire_bonjour_age_get.html rectifier action=, et remplacer method= " post " par method= " get "
	* Dans bonjour_age_get.php remplacer $_POST par $_GET

	Tester successivement formulaire_bonjour_age.html et formulaire_bonjour_age_get.html en observant l’adresse url de la barre de navigation. 

	Que constatez-vous ?

    ??? success "Solution"

        La méthode get fait circuler les informations du formulaire en clair dans la barre d'adresse.

???+ question "Que fait google ?"

	Dans un navigateur, par exemple Firefox, utiliser la barre de recherche google avec un mot : par exemple koala

	Quelle est la méthode utilisée par google ?

	??? success "Solution"

		On observe dans la barre d'adresse : https://www.google.com/search?client=firefox-b-d&q=koala

		google utilise donc la méthode get.
		

### 2. Comparaison des méthodes get et post

!!! info "Mon info"

	La méthode POST, transmet les informations du formulaire de manière masquée mais non cryptée. Le fait de ne pas afficher les données ne signifie en rien qu'elles sont cryptées. Rappelons nous d'ailleurs que ces informations utilisent le protocole HTTP et non HTTPS qui lui crypte les données.

	Quelle est la meilleure méthode à adopter alors ? Cela dépend. Le choix de l'une ou de l'autre se fera en fonction du contexte. Si par exemple, nous souhaitons mettre en place un moteur de recherches alors nous pourrons nous contenter de la méthode GET qui transmettra les mots-clés dans l'url. Cela nous permettra aussi de fournir l'url de recherches à d'autres personnes. C'est typiquement le cas des URLs de Google .

	La méthode POST est préférée lorsqu'il y'a un nombre important de données à transmettre ou bien lorsqu'il faut envoyer des données sensibles comme des mots de passe. Dans certains cas, seule la méthode POST est requise : un upload de fichier par exemple.

	[Source](https://apprendre-php.com/tutoriels/tutoriel-12-traitement-des-formulaires-avec-get-et-post.html)


## V. Crédits

Auteur : Mireille Coilhac





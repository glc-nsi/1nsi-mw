---
author: Mireille Coilhac
title: Les bases de PHP
---


## I. L'éditeur de texte

Jusqu'à présent, nous avons utilisé un éditeur de texte comme par exemple Notepad++ ou  Sublime Text.

``` mermaid
    graph LR
    	A[Editeur de texte PHP] --> C[Serveur Apache]
    	C --> B[Navigateur]
```

Dans cette page, nous n'allons pas utiliser UwAmp, mais nous allons tester les codes php en ligne sur le site : [w3schools](https://www.w3schools.com/php/phptryit.asp?filename=tryphp_intro){ .md-button target="_blank" rel="noopener" }


## II. Variables et affichages en php 

???+ question "Tester"

    Recopier et exécuter le script ci-dessous.

	* Les noms de variables commencent par un « $ »
	* `echo` est l’instruction d’affichage.
	* Le point « **.** » sert pour la concaténation.
	* Les lignes se terminent par un « ; »
	* `<br>`  sert à passer à la ligne.

	```php
	<?php
	$couleur1 = "rouge";
	$couleur2 = "noir";
	echo "Ma voiture est " . $couleur1 . "." ."<br>";
	echo "J'aime le " . $couleur1 ." et le ". $couleur2 . "." . "<br>";
	echo "A bientôt" . ".";
	?>
	```

???+ question "Tester"

	**1.** Recopier, puis tester le script suivant.  
	Rafraichir la page plusieurs fois.

	```html
	<html>
    <head>
        <title>Nombre aléatoire </title>
    </head>
    <body>
        <?php
        echo rand(1,6);
        ?>
    </body>
	</html>
	```

???+ question "Modifier"

	**2.** Ecrire le script qui produit un affichage analogue à celui-ci (le nombre obtenu est un entier aléatoire entre 1 et 6) :

	Si vous obtenez 6, vous avez gagné.  
	Vous avez obtenu : 6 

    ??? success "Solution"

        ```html
        <html>
    		<head>
        		<title>Ma loterie en ligne </title>
    		</head>
    		<body>
    			<?php
				echo "Si vous obtenez 6, vous avez gagné." . "<br>";
				echo "Vous avez obtenu : " . rand(1,6);
				?>
    		</body>
		</html>
		```

## III. les boucles while et foreach

???+ question "Tester `while`"

    Recopier, puis tester le script suivant.  
	Rafraichir la page plusieurs fois.

	// ou # sert à écrire des commentaires

	```html
	<html>
    <head>
        <title>Ma deuxième loterie </title>
    </head>
    <body>
        <h1>Les dés</h1>
        <p> On rejoue automatiquement, jusqu'à ce que 6 sorte. :</p>
        <?php				// Attention aux ;
		$de=rand(1, 6);      // $de désigne la variable $de
		while ($de<6)		// Observez la syntaxe du while
        {
			echo $de . "<br>";// ."<br>" permet de passer à la ligne
			$de=rand(1, 6);
		}
		echo $de . "<br>";
        ?>
    </body>
	</html>
	```

???+ question "Modifier"

	Modifier ce programme pour qu’il affiche au bout de combien de lancers de dé, on a obtenu le premier « 6 ».

	Exécuter votre script, puis rafraichir la page.

    ??? success "Solution"

    	```php
    	<?php
		# On rejoue automatiquement, jusqu'à ce que 6 sorte.

		$de=rand(1, 6);     # $de simule un lancer de de
		$tour=1;			# $tour contiendra le numéro du tour pour lequel
							# 6 sortira pour la première fois.
		while ($de<6)		
		{
			echo $de ."<br>";
			$de=rand(1, 6);
			$tour=$tour + 1;
		}
		echo $de ."<br>";	# En sortie de boucle $de contient 6.
		echo "Au tour numéro " . $tour ." le ". $de . " est sorti pour la première fois." ;
		?>
		```

???+ question "Tester `foreach`"

    Recopier, puis tester le script suivant.

    ```html
	<!DOCTYPE html>
	<html>
	<body>

	<?php  
	$colors = array("red", "green", "blue", "yellow");  # Un tableau

	foreach ($colors as $value) {
	  echo "$value <br>";
	}
	?>  

	</body>
	</html>
	```


## IV. Les tableaux associatifs

!!! info "Les tableaux associatifs"

    Les tableaux associatifs consistent en un ensemble de couples clé=>valeur, séparés entre eux par des virgules. On peut faire l’analogie avec les dictionnaires utilisée en Python. 

!!! example "Exemple"

    Ici, le tableau associatif donne le nombre de calories pour 100 g de différents aliments :

    ```php
    $calories = ["Pain au chocolat" => 410,
	"Miel" => 304,
	"Réglisse" => 377,
	"Sorbet" => 90,
	"Sucre" => 396,
	"Cookies" => 464];
	```
	Les valeurs sont retrouvées par la clé qui leur est associée. Par exemple, pour trouver le nombre de calories associées à 100 g de sorbet, vous utiliserez cette instruction :

	```php
	echo $calories["Sorbet"];
	```

	Pour parcourir tous les éléments du tableau, vous pouvez utiliser une boucle `foreach` crée pour parcourir les tableaux:

	```php
	foreach($calories as $cle => $valeur){
	echo $cle."=". $valeur.", "."<br>";}
	```

	Pour ajouter un couple clé=>valeur dans le tableau associatif :

	```php
	$calories["Pain blanc" ]= 265 ;
	```

???+ question "Exercice"

    Tester les syntaxes ci-dessus, puis créer un programme qui affiche la somme de tous les nombres de calories données dans le tableau.

    ??? success "Solution"

        ```php
        <?php
		$calories = ["Pain au chocolat" => 410,
		"Miel" => 304,
		"Réglisse" => 377,
		"Sorbet" => 90,
		"Sucre" => 396,
		"Cookies" => 464];
		$calories["Pain blanc" ]= 265 ;
		foreach($calories as $cle => $valeur){
		echo $cle."=". $valeur.", "."<br>";  # Pour passer à la ligne
		}
		$somme=0;
		foreach($calories as $cle => $valeur){
		$somme=$somme+$valeur;
		}
		echo $somme;
		?>
		```

	## V. Crédits

Auteur : Mireille Coilhac

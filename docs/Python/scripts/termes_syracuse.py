# --- PYODIDE:env --- #

def suivant(n):
    if n % 2 == 0:
        return n // 2
    else:
        return 3*n + 1

# --- PYODIDE:code --- #

def syracuse(n):
    ...

print(syracuse(5))


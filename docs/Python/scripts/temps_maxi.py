# --- PYODIDE:env --- #

def suivant(n):
    if n % 2 == 0:
        return n // 2
    else:
        return 3*n + 1

def temps_de_vol(n):
    compteur = 0
    while n != 1:
        compteur = compteur + 1
        n = suivant(n)
    return compteur

# --- PYODIDE:code --- #

def temps_max(n):
    maximum = 0
    for k in range(1, ...):
        duree = ...
        if duree ...:
            maximum = duree
            depart = ...
    return (maximum, depart)

(maxi, nbre_depart) = temps_max(100)
print('le plus grand temps de vol vaut :', maxi, "obtenu pour : ", nbre_depart)


# --- PYODIDE:corr --- #

def temps_max(n):
    maximum = 0
    for k in range(1, n + 1):
        duree = temps_de_vol(k)
        if duree > maximum:
            maximum = duree
            depart = k
    return (maximum, depart)


# --- PYODIDE:tests --- #

assert temps_max(100) == (118, 97)


# --- PYODIDE:secrets --- #

assert temps_max(200) == (124, 171)


# On importe la fonction randint du module random
from random import randint

# Bob lance sa flechette, elle atteint une case au hasard du plateau :
numero_ligne = randint(0, 4)
numero_colonne = randint(0, 4)

print("La flechette atteint la case située : Ligne :", numero_ligne,"et  Colonne :", numero_colonne)
# a-t-il gagné ?

if ...
    print("Bob a gagné")
elif ...
    print("Bob a gagné")
... :
    print("Bob à perdu")

# --- PYODIDE:env --- #
from js import document
if "restart" in globals():
    restart()

# --- PYODIDE:code --- #
from turtle import *
forward(100)
left(90)
forward(100)
left(90)
# --- PYODIDE:post --- #
done()
document.getElementById("cible_1").innerHTML = svg()
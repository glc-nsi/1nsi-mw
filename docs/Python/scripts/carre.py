def fonction_carre(x):
    """
    renvoie la carré de x
    """
    return x**2

assert fonction_carre(3) == 9

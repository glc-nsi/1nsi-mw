# --- PYODIDE:code --- #

def somme(a,b):
    return ...


# --- PYODIDE:corr --- #

def somme(a,b):
    return a+b



# --- PYODIDE:tests --- #

assert somme(1, 1) == 2


# --- PYODIDE:secrets --- #

assert somme(2, 7) == 9
assert somme(30,2.1) == 32.1


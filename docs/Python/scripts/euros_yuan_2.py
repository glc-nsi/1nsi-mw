def euros_vers_dollars(montant):
    return montant * 1.19 # en supposant qu'un euro vaut 1,19 dollars

def dollars_vers_yuans(montant):
    return montant * 6.93 # en supposant qu'un dollar vaut 6,93 yuans

def euros_vers_yuans(montant):
    # Appel de la fonction euro_vers_dollar
    montant_dollar = euros_vers_dollars(montant)
    # Appel de la fonction dollar_vers_yuan
    montant_yuan = dollars_vers_yuans(montant_dollar)
    # Valeur renvoyee
    return montant_yuan

montant_converti = euros_vers_yuans(2)
print(montant_converti)



---
author: Mathias Weislinger
title: Entraînements à Python
---

Sur cette page vous trouverez  divers ressources dont divers TP étudiés en classe.


## Mes activités sur Capytale

??? info "Le jeu du nombre mystère"
    Écrivez et testez un programme ayant pour fonction principale une fonction sans argument se nommant ```jeu_du_nombre_mystere```. En appelant cette fonction, voici ce qu’il devra se passer :

    *  l’ordinateur choisira  un nombre entier au hasard entre 1 et 50 ;
    *  l’ordinateur demandera au joueur de le trouver ;
    *  le joueur aura le droit à autant d’essai qu’il le souhaite ;
    *  la machine dira à chaque essai si le nombre mystère est plus petit ou plus grand que le nombre choisi par le joueur.
    
              
    Quand le joueur a trouvé le nombre mystère le programme affiche un message de félicitations et le nombre de tentatives effectuées.

    ??? warning "Indication"
        on importera le module ```random``` en début de script qui permet d’utiliser des fonctions génératrices des nombres aléatoires.  
        Cette bibliothèque contient une fonction ```randint```que vous utiliserez.
        
        ??? tips "Voir la documentation"
            * Rappel des instructions à taper dans une console :

            ```pycon

                >>> import random
                >>> help(random.randint)

            ```

            * Voici une illustration de la documentation :

            <div align="center">![help random](images/help-random.png){ width=60% }</div>


            * Visualiser vous-même la documentation dans la console ci-dessous :

            {{ terminal() }}
        


    <!--{{ terminal('scripts/help-random.py' , FILL='help(random.randint)')}}-->
    
    ??? note "**Illustration du travail une fois le programme terminé** "
        <p><img style="vertical-align:middle; width:60%;" alt="sortie_nb_mystere"  src="../images/nb-mystere-sorties.png"/></p>


    !!! info "**Ce travail est à rendre et il sera réalisé dans le notebook suivant :**"
     
        <div align="center"> 👉 [https://capytale2.ac-paris.fr/web/c/556b-4265033/edn](https://capytale2.ac-paris.fr/web/c/556b-4265033/edn)</div>
    
        _Vous devrez présenter le problème à résoudre dans le notebook en vous familiarisant un peu au markdown et proposer une solution en python basée sur les connaissances de bases de  première._ 

    ??? note "**Voir la version pdf**"
        <div class="centre">
        <iframe 
        src="../pdf/devoir-nombre-mystere.pdf"
        width="1000" height="1000" 
        frameborder="0" 
        allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
        </iframe>
        </div>


<!--**Voir la version pdf**:
![PDF du travail à faire](pdf/devoir-nombre-mystere.pdf){ type=application/pdf style="min-height:25vh;width:100%" }-->

## Autres ressources

??? info "CodEx"
    Ce site propose **des exercices d'apprentissage de l'algorithmique et de la programmation en ```Python```** par le biais d'exercices variés. Les exercices proposés ont été écrits, testés, corrigés et améliorés par des professeurs d'informatique du secondaire et du supérieur.  
    Aucune installation, aucune inscription ne sont nécessaires : tous les programmes sont exécutés sur votre

    <p style="text-align:center;"> 👉 [Visiter CodEX](https://codex.forge.apps.education.fr/)</p>

	

---
author: Mathias Weislinger
title: Installer Python
---

## I. Spyder

* Nous allons souvent travailler en classe avec Spyder en installant une distribution très complète et très utilisée dans le monde scientifique :

  👉 Lien de téléchargement : [https://www.anaconda.com/download/success](https://www.anaconda.com/download/success){ .md-button target="_blank" rel="noopener" }

## II. Autres IDE

### EduPython 

* Si vous avez des difficultés avec Spyder vouspouvez optez pour Edupython :  

👉 Lien de téléchargement : [https://edupython.tuxfamily.org/](https://edupython.tuxfamily.org/){ .md-button target="_blank" rel="noopener" }

* En bas de la page cliquer sur « Télécharger la dernière version »


### Thonny

Si vous ne parvenez pas à installer ni Spyder, ni EduPython, vous pouvez installer Thonny.

👉 Lien de téléchargement : [https://thonny.org/](https://thonny.org/){ .md-button target="_blank" rel="noopener" }

Il y a moins de bibliothèques installées, mais certains outils sont très pratiques : 

* vérificateur de variables en haut à droite
* un débugueur
* un système d’évaluation des expressions étape par étape

## III. Les notebooks Jupyter

👉 [Utiliser jupyter](../outils/utiliser_jupyter.md)
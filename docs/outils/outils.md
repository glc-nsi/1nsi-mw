---
author: Mathias Weislinger
title: Les outils en NSI
--- 

## Site d'exercices CodEx (première et terminale)

[CodEx](https://codex.forge.apps.education.fr/){ .md-button target="_blank" rel="noopener" }

## Spyder, EduPython, basthon, Thonny, replit ou autre …

!!! info "Environnement de développement"

    Spyder, EduPython, basthon, Thonny, replit sont des environnements de développement. Essentiellement vous y trouvez : 

	* La console : on peut entrer des instructions directement exécutées.
	* L'éditeur de code : on y saisit des codes. 
	
	👉 Les affichages produits par le code sont affichés dans la console.

## Les notebooks Jupyter

!!! info "Ouvrir un fichier .ipynb"

	Vous pouvez facilement ouvrir un notebook Jupyter avec JupyterLab via Anaconda ou via EduPython  ou en ligne
	[Utiliser jupyter](../outils/utiliser_jupyter.md)

!!! info "Les cellules"

	* 3 façons pour exécuter les cellules : barre d'outils, majuscules+Entrée, Ctrl+Entrée
	* Les cellules ne sont pas indépendantes : c'est comme un seul code écrit en plusieurs parties. 

## Les sauvegardes et votre Cloud

!!! warning "Attention"

	👉 Les sauvegardes ne sont pas automatiques : **Pensez à sauvegarder**

	Bouton de la barre d'outil pour sauvegarder :  
	Menu Fichier -> sauvegarder sous... pour enregistrer un fichier (ou File puis Save as) 

!!! info "S'organiser"

	👉 Toujours veiller à organiser les sauvegardes :

	* Sur votre clef USB : créez les bons répertoires et enregistrez les documents au bon endroit
	* **Sur votre Cloud : enregistrez les documents au bon endroit à chaque séance**


## Les messages d’erreurs de vos codes

!!! info "Les messages d'erreurs"

	👉 La lecture de ces messages est indispensable.  

	👉 C'est le principal travail d'un programmeur : mettre au point le code.

	😊 Nous aurons l'occasion, tout au long de l'année, de voir tous les messages d'erreurs fréquents, apprendre à les comprendre, et corriger le code.

!!! info "Les numéros de lignes"

	Dans un notebook, les n° de lignes n'apparaissent pas forcément. 

	Pour les faire apparaitre : 

	Menu Affichage -> afficher/masquer les n° de lignes

## Repères historiques

<iframe title="Video histoire de l'informatique - YouTube-2" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/63087e63-fbc8-46b2-8349-7b5a54e6d3f2" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/PH-Qjy4zVKQ" title="Documentaire sur l&#39;histoire de l&#39;informatique - Les cinglés de l&#39;informatique en intégral" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<!-- 
<iframe width="560" height="315" src="https://www.youtube.com/embed/dOakyAhqiVY?si=CnzkSVktA2ohlae7" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/zywPwbQqshY?si=q9mRySkWJ_Rkl8Lc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

--> 
---
author: Mathias Weislinger
title: Notebook Jupyter
--- 

## I. Jupyter sur votre ordinateur

??? info "Avec Anaconda"

	Vous pouvez utiliser un notebook Jupyter (fichier avec l'extension .ipynb) 
	sur votre ordinateur en lançant JupyterLab via **Anaconda-Navigator** :
	
	* Lancer Anaconda-Navigator 
	* Lancer JupyterLab

	![Lancer Anaconda-Navigator puis lancer JupyterLab](images/jupyter-lab0.png){ width=90% }

    * Utiliser l'explorateur intégré pour **ouvrir un notebook** ou **créer un nouveau notebook**

	![Créer un notebook](images/jupyter-lab1.png){ width=90% }


??? info "Avec EduPython"

	Vous pouvez utiliser un notebook Jupyter (fichier avec l'extension .ipynb) sur votre ordinateur sur Edupython, en ouvrant le menu Outils>Jupyter>  ou en cliquant directement ici : 
	
	![Ouvrir sur EduPython](images/ouvrir_jupy_edupython.png){ width=90% }
	
	![clic dossier](images/clic_dossier.png){ width=90% }

## II. Jupyter en ligne 

??? info "Via votre ENT"
	👉 [Accès direct à Capytale via l'Éduc de Normandie](https://capytale2.ac-paris.fr/web/c-auth/pvd/edn/connect){ .md-button target="_blank" rel="noopener" }

??? info "Utiliser Basthon en cas de panne de l'ENT par exemple"

	😉 basthon signifie **ba**c à **s**able pour Py**thon** 3.
	
	!!! info "Sur tablette"
	
	😊 basthon est un outil en ligne, vous pourrez donc sans aucune installation l'utiliser sur votre tablette.
	
	
	👉 Suivre le lien :  [https://basthon.fr/](https://basthon.fr/){ .md-button target="_blank" rel="noopener" }
	
	👉 Cliquer sur Console, puis sur Python
	
	![basthon](images/basthon.png){ width=70% }
	
	Vous obtenez à gauche l’éditeur et à droite la console.
	En bas le menu vous permet d’exécuter, ouvrir, **télécharger** ce que vous avez fait (**il faudra absolument le faire pour sauvegarder votre travail dans votre Cloud**) etc.
	Vous pouvez essayer ce qui est proposé dans cette barre d’outils, en particulier l'icône partager.
	
	![console](images/console.png){ width=70% }
		
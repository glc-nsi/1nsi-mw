---
author: Jean-Louis Thirot, Valérie Mousseaux, Fabrice Nativel et Mireille Coilhac
title: CSS
---

## I. Le fichier CSS : principe général

!!! info

	Un document **html** est presque toujours accompagné d'au moins un fichier **CSS** qui décrit la mise en page,
	les formats (polices, couleurs, styles des éléments...). Un même fichier **CSS** peut être utilisé dans plusieurs pages d'un
	même site, ce qui permet, en le modifiant, de changer la présentation de toutes les pages (sinon il faudrait les modifier
	une à une). Mais ceci n'est qu'un aspect de l'intérêt des feuilles de styles. 

	👉 Essentiellement, elles ont pour rôle de permettre
	de séparer le contenu (qui est dans le html) de la forme (qui est dans le css)
	


### Sélecteur / Propriété / Valeur


Le CSS permet d'appliquer des styles sur différents éléments sélectionnés dans un document HTML. Par exemple, 
on peut sélectionner tous les éléments d'une page HTML qui sont paragraphes et afficher leurs textes en rouge avec 
ce code CSS :

!!! example "Exemple"

	```html
	p {
  		color: red;
	}
	```

!!! info

	Il y a 3 éléments dans la syntaxe :

	* **`p`** est le **sélecteur** : indique à quels éléments doit on applique le style. Ici, ce seront tout les éléments **`p`**, donc tous les paragraphes.
	* **`color`**est la **propriété** : indique (pour les éléments sélectionnés), quelle propriété on veut modifier.
	* **`red`** est la **valeur** qu'on attribue à la propriété pour les éléments sélectionnés.


### Où mettre le code CSS ?

!!! abstract "En bref"

    Il est possible de l'inclure dans la page html mais il est recommandé de mettre le code CSS dans un autre fichier,
    ou même parfois dans plusieurs fichiers, chacun contenant des instructions de formatage spécifiques. 


### Comment inclure le CSS dans la page ?

!!! info "`<link>`"

	  Etant donné qu'on place les instructions de style dans un fichier annexe, il est nécessaire de l'indiquer dans la page html.  
	  On utilise pour cela l'instruction `<link>` entre `<head>` et `<\head>`

!!! abstract "Un exemple"

    ```html 
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="mon_styles.css" type="text/css">
        <title>Titre de l'onglet</title>
    </head>

    <body>

    </body>
    </html>
    ```


???+ note "Précisions"

    * **`rel=`** définit le type de relation, ici stylesheet indique un lien vers une feuille de style.
	* **`href=`** donne le nom du fichier, qui peut être un chemin absolu, relatif, ou une adresse web.
	* **`type=`** définit le type de contenu, pour les feuilles de styles, c'est text/css.</li>


## II. Quelques propriétés fréquentes

Nous allons donner ici quelques syntaxes usuelles.
Il en existe beaucoup d'autres.  
Vous pourrez approfondir plus tard en suivant ce lien (on peut choisir comme langue le français) : [w3 schools](https://www.w3schools.com/css/default.asp){ .md-button target="_blank" rel="noopener" }

### 1. Pour les textes

!!! info "color : couleur du texte "

    Pour mettre les titres de niveau 1 en rouge

    ![titre rouge](images/titre_rouge.png){ width=100% .center}


!!! info "font-family : pour utiliser différentes polices "

    Pour mettre les paragraphes en police Monotype Corsiva .

    ![police](images/police.png){ width=100% .center}


!!! info "font-size : pour modifier la taille du texte"

    La taille en `1em` est la taille normale utilisée sur le client. `2em` sera **2 fois** plus grand.

    ![taille caractères](images/taille_font.png){ width=100% .center}


!!! info "font-style : pour mettre par exemple en italique"

    La valeur peut être : normal, italic ou oblique

    ![font style](images/font_style.png){ width=100% .center}


!!! info "text-align : pour centrer/justifier à gauche ou droite"

    La valeur peut être : center, left ou right

    ![alignement](images/alignement.png){ width=100% .center}


### 2. Pour la la page

!!! info "background : couleur du fond"

    Pour mettre un fond gris :

    ![fond](images/fond.png){ width=100% .center}


### 3. Pour une image

!!! info "width : largeur de l'image (en % ou en px)"

    Dans l'exemple suvant l'image occupe 10 % de la largeur de la page. Elle est donc très petite !
    
    ![taille image](images/taille.png){ width=100% .center}


### 4. Pour un lien ou un menu

!!! abstract "Un exemple"

    Considérons le code html suivant :

    ```html
    <nav>
      <ul>
      <li><a href="accueil.html">Accueil<a></li>
      <li><a href="Projets.html">Projets<a></li>
      <li><a href="quisommesnous.html">qui sommes nous</a></li>
      </ul>
    </nav>
    ```

L'apparence sans CSS est la suivante : 

![menu sans css](images/menu_sans_css.png){ width=15% .center}

!!! abstract "Ajoutons ceci dans notre fichier css :"

    ```html
    nav {
      width: 200px;
      list-style: none;
      margin: 0;
      padding: 0;
    }
    nav li {
      background: #c00 ;
      color: #fff ;
      border: 1px solid #600 ;
      margin-bottom: 1px ;
    }
    nav li a {
      display: block ;
      background: #c00 ;
      color: #fff ;
      font: 1em "Trebuchet MS",Arial,sans-serif ;
      line-height: 1em ;
      text-align: center ;
      text-decoration: none ;
      padding: 4px 0 ;
    }
    ```

L'apparence **avec** CSS est la suivante : 

![menu avec css](images/avec_css.png){ width=30% .center}

## III. Exercice

???+ question "Modifier le style"

    Enregistrer le fichier suivant : `Le rougail de saucisess` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/rougailsaucisses.html)

    1. Créer une feuille de style dans un fichier séparé, modifier l'apparence de cette page web de sorte que:

        * Le fond de la page soit de couleur bleu ciel (lightblue).
        * Les caractères de la page en bleu
        * Les titres de niveau 1 soient en vert.
        * Les titres de niveau 2 soient en violet.

        ??? success "Le fichier mon_style.css"

            ```html
            body {
	            background : lightblue;
	            color : blue;
            }

            h1 {
	            color : green;
            }

            h2 {
	            color : purple;
            }
            ```

    2. Créer le lien dans le fichier html vers la feuille de style.

        ??? success "Le lien"

            Début du fichier : 

            ```html
            <!DOCTYPE html>
            <html>
            <head>
                <meta charset="utf-8">
                <link rel="stylesheet" href="mon_style.css" type="text/css">
                <title>A la gloire du rougail saucisses</title>
            </head>
            ```

    3. Vérifiez le rendu avec votre navigateur.

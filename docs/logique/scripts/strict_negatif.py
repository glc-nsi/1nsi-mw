def est_strictement_negatif_1(nbre):
    if nbre < 0:
        return True
    else:
        return False
        
def est_strictement_negatif_2(nbre):
    return nbre < 0

print("est_strictement_negatif_1(3) : ", est_strictement_negatif_1(3))
print("est_strictement_negatif_1(0) : ", est_strictement_negatif_1(0))
print("est_strictement_negatif_1(-3) : ", est_strictement_negatif_1(-3))
print("est_strictement_negatif_2(3) : ", est_strictement_negatif_2(3))
print("est_strictement_negatif_2(0) : ", est_strictement_negatif_2(0))
print("est_strictement_negatif_2(-3) : ", est_strictement_negatif_2(-3))


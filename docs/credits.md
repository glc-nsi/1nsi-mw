---
author: Mathias Weislinger
title: 👏 Crédits
---

😀 Je remercie vivement **Mireille Coilhac** pour son excellent [site](https://mcoilhac.forge.apps.education.fr/site-nsi/)  dont mon site est un **fork** .

En particulier :

Le chapitre sur les tableaux a été réalisé par N. Revéret, Pierre Marquestaut, Jean-Louis Thirot et Mireille Coilhac.
L'ensemble des documents sont sous licence [CC-BY-SA 4.0 (Attribution, ShareAlike)](https://creativecommons.org/licenses/by-sa/4.0/){target="_blank"}.

Le chapitre sur les dictionnaires a été réalisé par Mireille Coilhac avec la contribution de Charles Poulmaire.

Le chapitre HTML et CSS  a été réalisé par Fabrice Nativel, Jean-Louis Thirot, Valérie Mousseaux et Mireille Coilhac.

Le site est hébergé par la forge des communs numériques éducatifs <a href="https://docs.forge.apps.education.fr/">
<span aria-label="Avatar" aria-hidden="true" data-type="round" data-color="3" class="_avatar_k41ul_17 mx_BaseAvatar" style="--cpd-avatar-size: 16px;"><img loading="lazy" alt="" src="https://matrix.agent.education.tchap.gouv.fr/_matrix/media/v3/thumbnail/matrix.agent.education.tchap.gouv.fr/de0e2fe63b40dd452178360baa3ff29ba16d8b98?width=16&amp;height=16&amp;method=crop" crossorigin="anonymous" referrerpolicy="no-referrer" class="_image_k41ul_49" data-type="round" width="16px" height="16px"></span><span class="mx_Pill_text">Centre de documentation</span></a>

![AEIF](./assets/images/logo_aeif_300.png){width=7%}    
Le modèle du site a été créé par l'  [Association des enseignantes et enseignants d'informatique de France](https://aeif.fr/index.php/category/non-classe/){target="_blank"}. 

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/), et surtout [Pyodide-Mkdocs-Theme](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/) pour la possibilité d'intégrer des exercices Python et pour les QCM.


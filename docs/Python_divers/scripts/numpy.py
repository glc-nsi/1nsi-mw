import numpy as np  # np est un 'alias'. Il suffira d'écrire np au lieu de numpy
# On crée une liste de 5 nombres régulièrement répartie entre 0 et 10
lst = np.linspace(0, 10, 5)
print(lst)


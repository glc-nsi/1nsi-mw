from statistics import *
from random import randint
from numpy import percentile # pour les quartiles

data = [randint(1, 1000) for i in range (10000)]
print("nombre de '500' : ", data.count(500) )
moyenne = mean(data)
minimum = min(data)
maximum = max(data)
mediane = median(data)
ecart_type = stdev(data)
variance = variance(data)
q1 = percentile(data, 25)
q3 = percentile(data, 75)

print("moyenne : ", moyenne)
print("mediane : ", mediane)
print("minimum : ", minimum)
print("maximum : ", maximum)
print("ecart type : ", ecart_type)
print("variance : ", variance)
print("Q1 = ", q1)
print("Q3 = ", q3)


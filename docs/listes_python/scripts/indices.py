# La liste étudiée
meubles = ['Table', 'Commode', 'Armoire', 'Placard', 'Buffet']

# Saisir l'indice permettant d'afficher 'Armoire'
print(meubles[...])

# Saisir l'instruction permettant d'afficher 'Buffet'
...

# Saisir l'instruction permettant d'afficher la longueur du tableau
...

# Compléter la boucle afin d'afficher tous les meubles du tableau
# comme ci-dessous :
# Table, Commode, Armoire, Placard, Buffet,
for indice in range(...):
    print(...[...])

    # Tests
    m = [[1, 3, 4], 
     [5, 6, 8], 
     [2, 1, 3]]
    assert accede_valeur(m, 0, 0) == 1
    assert accede_valeur(m, 1, 2) == 8

    # Autres tests
    m2 = [[0, 1, 2, 3, 4, 5, 6],
    [7, 8, 9, 10, 11, 12, 13],
    [14, 15, 16, 17, 18, 19, 20]]

    for i in range(3):
        for j in range(7):
            assert accede_valeur(m2, i, j) == m2[i][j]

    

# --- PYODIDE:code --- #

def moyenne(lst_notes):
    """
    lst_notes est une liste de tuples (note, coefficient)
    La fonction renvoie la moyenne des notes en tenant compte des coefficients
    """
    ...


# --- PYODIDE:corr --- #

def moyenne(lst_notes):
    """
    lst_notes est une liste de tuples (note, coefficient)
    La fonction renvoie la moyenne des notes en tenant compte des coefficients
    """
    somme_notes = 0
    somme_coefs = 0
    for (note, coef) in lst_notes:
        somme_notes = somme_notes + note*coef 
        somme_coefs = somme_coefs + coef 
    resultat = round(somme_notes/somme_coefs, 2)
    return resultat


# --- PYODIDE:tests --- #

assert moyenne([(15, 1), (12, 1), (14, 2)]) == 13.75
assert moyenne([(15, 3)]) == 15


# --- PYODIDE:secrets --- #

assert moyenne([(12, 2), (18, 1), (14, 3), (13, 4)]) == 13.6


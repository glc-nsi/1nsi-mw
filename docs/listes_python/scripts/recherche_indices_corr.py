def indices(element, entiers):
    return [i for i in range(len(entiers)) if entiers[i] == element]
    
---
author: Mireille Coilhac
title: Affectations et listes  
---

# un professeur veut relever ses notes

Un professeur a relevé la liste des notes obtenues dans un groupe à une évaluation. (`liste_1`).

Il désire relever toutes les notes d’1 point, mais il veut garder pour lui la liste originale.

Voici son script et le résultat obtenu à l’exécution :


???+ Testez

    `liste_2 = liste_1`

    {{IDE('scripts/copie_1')}}


🌵 On observe que le professeur a « perdu » la liste originale `liste_1`, qui est devenu identique à la `liste_2`.

👉 En effet, tout se passe comme si l’instruction `liste_2 = liste_1` donnait deux noms différents au même objet.

`liste_2` n’est pas une nouvelle liste.

Il y a plusieurs syntaxes possibles pour remédier à ce problème : en voici quelques unes.


???+ Testez

    `liste_2 = list(liste_1)`

    {{IDE('scripts/copie_2')}}


???+ Testez

    `liste_2 = liste_1[:]`

    {{IDE('scripts/copie_3')}}



???+ Testez

    `liste_2 = liste_1.copy()`

    {{IDE('scripts/copie_4')}}



???+ Testez

    `liste_2 = [note for note in liste_1]` avec une liste en compréhension

    {{IDE('scripts/copie_5')}}



# Un autre exemple avec des fruits :


<iframe width="100%" height="410" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=fruits_1%20%3D%20%5B%22Pomme%22,%20%22Poire%22%5D%0Afruits_2%20%3D%20fruits_1%0A%20%20%20%20%0A%23%20Modification%20de%20fruit_2%0Afruits_2%5B0%5D%20%3D%20%22Fraise%22&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=3&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>


🌵 Comme on peut le voir, lors de sa création, `fruits_2` pointe vers le **même objet** de type `list` : `fruits_1`. 

😏 Le fait de modifier `fruits_2`, modifie donc forcément `fruits_1`.

👉 **Ce mécanisme est très important et il faudra le garder à l'esprit.**





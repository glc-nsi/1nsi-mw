---
author: Mireille Coilhac
title: Les dictionnaires - À vous
---


!!! info "`mon_dico`"

    On appelle `mon_dico` le dictionnaire qui servira pour expliquer les syntaxes.

!!! abstract "Ajouts de couples (clé, valeur)"

    Il suffit de faire une nouvelle affectation : 


!!! abstract "Appartenance d'une clé dans un dictionnaire"

    * $\hspace{10em}$ renvoie `True` si la clé `cle` existe dans `mon_dico` et `False` sinon.  
    * $\hspace{10em}$ renvoie `True` si la clé `cle` n'existe pas dans `mon_dico` et `False` sinon. 


!!! abstract "Accéder à une valeur"

    $\hspace{10em}$ renvoie la valeur associée à `cle` si elle est présente dans le dictionnaire, sinon une erreur `KeyError` se produit.
    

!!! abstract "Modifier une valeur"
 
    Il suffit de faire une nouvelle affectation : 


!!! abstract "Supprimer une valeur"

    Soit `valeur` la valeur associée à `cle`.

    * $\hspace{10em}$ supprime le couple (`cle`, `valeur`) de `mon_dico`.
    * $\hspace{10em}$ supprime le couple (`cle`, `valeur`) de `mon_dico` et renvoie la valeur correspondante.
    

!!! abstract "Ajouter un couple (`cle`, `valeur`)"

    Soit `valeur` la valeur que l'on souhaite associer à `cle`.  

    * Si la clé existe déjà $\hspace{15em}$ modifie la valeur associée,
    * sinon $\hspace{15em}$ ajoute la paire (`cle`, `valeur`)
    

!!! abstract "Longueur d'un dictionnaire"

    $\hspace{10em}$ renvoie le nombre de couple (`cle`, `valeur`) du dictionnaire.


!!! abstract "Parcourir un dictionnaire"

    Le parcours avec la boucle $\hspace{15em}$ permet de pacourir les clés de `mon_dico`


!!! abstract  "Utiliser les méthodes `keys`, `values` et `items`"

    On peut parcourir les vues créées par ces méthodes, de façon analogue à ce que l'on ferait avec d'autres séquences comme des listes :
    
    * $\hspace{15em}$ permet d'accéder à toutes les clés de `mon_dico`
    * $\hspace{15em}$ permet d'accéder à toutes les valeurs de `mon_dico`
    * $\hspace{15em}$ permet d'accéder à tous les couples (clé, valeur) de `mon_dico`
    

!!! abstract  "obtenir des listes de clés, valeurs, paires (clé, valeur)"

    On peut créer les listes de clés, de valeurs ou de couples (clé, valeur) :
    
    * $\hspace{15em}$ permet dobtenir une liste des clés de `mon_dico`
    * $\hspace{15em}$ permet dobtenir une liste des valeurs de `mon_dico`
    * $\hspace{15em}$ permet dobtenir une liste des tuples (clé, valeur) de `mon_dico`


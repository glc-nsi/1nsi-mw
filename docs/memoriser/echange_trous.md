---
author: Mireille Coilhac
title: Algorithme d'échange - À vous
---

!!! info "Echanger le contenu de deux variables"

	Nous désirons échanger le contenu des variables `a` et `b`

## Avec une variable temporaire 

```python
a = valeur_de_a
b = valeur_de_b
# problème : echanger les deux valeurs
...
...
... 
```

## En utilisant des tuples

!!! info "💡 A connaître"

	👉  Il est aisé, en python, de faire cet échange de façon très simple
	

	```python
	...   
	   
	```


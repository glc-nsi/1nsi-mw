---
author: Mireille Coilhac
title: Méthodes de listes Python - À vous
---


!!! warning "Remarque"

    Il y a toujours des parenthèses, éventuellement vides, pour les méthodes.
    Les méthodes utilisent la notation pointée.


## I. Les indispensables

!!! abstract "$\hspace{15em}$"

    Ajoute un élément à la fin de la liste. 


!!! abstract "$\hspace{15em}$"

    Supprime de la liste le **premier** élément dont la **valeur** est égale à `x`. 


!!! abstract "$\hspace{15em}$"

    * Enlève de la liste l'élément situé à l'indice `i` et renvoie cet élément.
    * Si aucun indice n'est spécifiée, $\hspace{5em}$ enlève et renvoie le dernier élément de la liste.


!!! abstract "$\hspace{15em}$"

    Ordonne les éléments dans la liste. La liste est modifiée (triée).  

## II. L'instruction 

!!! abstract "$\hspace{15em}$"

    Enlève de la liste l'élément situé à l'indice `i` **mais ne renvoie pas** cet élément


## III. non indispensable

!!! abstract "$\hspace{15em}$"

    Renvoie le nombre d'éléments ayant la valeur `x` dans la liste.


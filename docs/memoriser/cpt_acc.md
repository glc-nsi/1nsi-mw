---
author: Jean-Louis Thirot et Mireille Coilhac
title: Compteurs accumulateurs 
---

## I. Compteur

!!! info "Compter" 

	Une variable compteur est utile pour compter. 

	* Cette variable est initialisée avant la boucle (valeur de départ, pas toujours nulle).
	* On itère (via une boucle for par exemple) : bien définir les bornes de l'itération et l'incrément.
	* Dans le corps de la boucle, on incrémente (c'est à dire on ajoute 1) la valeur du compteur.

!!! example "Exemple 1"

    ```python
    compteur = valeur_initiale  #  (1) 
                                              
	for i in range(...):  # (2)          
        instructions 
        compteur = compteur + 1  # (3)
    
	print(compteur)	 # (4)		
	```

	1. Initialisation de la valeur avant la boucle (souvent à 0, mais pas obligatoirement)

	2. Itération : bien définir les bornes de début et fin

	3. On compte : 	dans le corps de la boucle on ajoute 1 à la variable `compteur`

	4. On a compté le nombre de fois que le bloc instructions a été exécuté

	!!! warning "Prenez le temps de lire les commentaires (cliquez sur les +)"


!!! example "Exemple 2"	

	```python
	compteur = valeur_initiale  # (1)
                                              
	for i in range(...):  # (2)
    	if condition :
        	compteur = compteur + 1   # (3) 
    
	print(compteur)  # (4)
	```

	1. Initialisation de la valeur avant la boucle (souvent à 0, mais pas obligatoirement)

	2. Itération : bien définir les bornes de début et fin

	3. On compte : 	dans le corps de la boucle on ajoute 1 à la variable `compteur`
	
	4. On a compté le nombre de fois que la condition a été réalisée.

	!!! warning "Prenez le temps de lire les commentaires (cliquez sur les +)"


## II. Accumulateur

!!! info "Accumulateur" 

	Une variable accumulateur et utile pour calculer une somme, un produit, et nous verrons d'autres situation comparables.

	* Cette variable est initialisée avant la boucle (valeur de départ, pas toujours nulle).
	* On itère (via une boucle for par exemple) : bien définir les bornes de l'itération et l'incrément.
	* Dans le corps de la boucle, par ajouts (ou autre opération) successifs, on modifie la valeur de l'accumulateur.

!!! example "Exemple 1 : addition"

	```python
	total = 0                   # (1)
	for i in range(1, n + 1):   # (2)
    	total = total + i       # (3)

	print(total)
	```

	1. Initialisation de l'accumulateur

	2. Itération : ici, i varie de 1 à `n`

	3. Instructions : on ajoute successivement les valeurs de `i` dans l'accumulateur

	!!! warning "Prenez le temps de lire les commentaires (cliquez sur les +)"

!!! example "Exemple 2 : multiplication"

	```python
	produit = 1                 # (1)
	for i in range(1, n + 1):   # (2)
    	produit = produit * i   # (3)

	print(produit)
	```

	1. Initialisation de l'accumulateur

	2. Itération : ici, i varie de 1 à `n`

	3. Instructions : on multiplie successivement les valeurs l'accumulateur par i


	!!! warning "Prenez le temps de lire les commentaires (cliquez sur les +)"

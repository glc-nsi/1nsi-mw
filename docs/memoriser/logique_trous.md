---
author: Mireille Coilhac
title: Logique - À vous
---

# Le type booléen

En 1847, George Boole invente une algèbre pour formaliser la logique.
Il définit trois opérateurs de base, qui vont permettre de traiter tous
les problèmes de logique.


## I. Exemples de propositions et opérateurs de bases

!!! example "Exemple"

	a : Au self, il y a des frites tous les jours  
	b : Il est interdit de fumer dans le lycée  


!!! info "Une proposition peut être vraie ou fausse"

	Vrai : True ou 1  
	Faux : False ou 0

???+ question "Donner la valeur de vérité des propositions a et b"

	a : Au self, il y a des frites tous les jours  
	b : Il est interdit de fumer dans le lycée 

    a est une proposition ...

    b est une proposition ...


???+ question "Opérateur **ou**"

	a : Au self, il y a des frites tous les jours   
	b : Il est interdit de fumer dans le lycée  
	a ou b est une proposition ...

	On peut noter a or b, ou a $\vee$ b (correspond au symbole $\cup$)

    a ou b est une proposition ...


???+ question "Opérateur **et**"

	a : Au self, il y a des frites tous les jours   
	b : Il est interdit de fumer dans le lycée   
	a et b est une proposition ...

	On peut noter a and b, ou a $\wedge$ b (correspond au symbole $\cap$)

    a et b est une proposition ...


???+ question "Opérateur **non**"

	a : Au self, il y a des frites tous les jours   
	b : Il est interdit de fumer dans le lycée  
	non a : ...   
	non b : ... 

	On peut noter not a ou  $\neg$a

    non a est une proposition ... 

    non b est une proposition ...


## II. Tables de vérités

???+ question "On jette deux dés"

	Des propositions peuvent être vraies ou fausses.  
	Par exemple : on jette deux dés.    
	x : le résultat du premier dé est pair  
	y : le résultat du deuxième dé est pair  

	Arbre de toutes les possibilités : 

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

!!! info "Présentation des tables de vérités"

    La présentation usuelle reprend l'ordre trouvé avec l'arbre ci-dessus :

    * Pour une seule proposition x:

    |x| ... | 
    |:--:|:--:|
    |0|...|
    |1|...|

    * Pour deux propositions x et y

    |x|y| ... | 
    |:--:|:--:|:--:|
    |0|0|...|
    |0|1|...|
    |1|0|...|
    |1|1|...|

???+ question "Table de vérité de l'opérateur non"

	Compléter la table de vérité

	|$x$| $\overline{x}$ | 
    |:--:|:--:|
    |0|...|
    |1|...|


???+ question "Table de vérité de l'opérateur ou"

	Compléter la table de vérité

    |x|y| x $\vee$  y| 
    |:--:|:--:|:--:|
    |0|0|...|
    |0|1|...|
    |1|0|...|
    |1|1|...|

    ![parallele](images/parallele.png){ width=30%; : .center }

	On peut faire l'analogie avec des interrupteurs en parallèle.


???+ question "Table de vérité de l'opérateur et"

	Compléter la table de vérité

    |x|y| x $\wedge$  y| 
    |:--:|:--:|:--:|
    |0|0|...|
    |0|1|...|
    |1|0|...|
    |1|1|...|

    ![serie](images/serie.png){ width=30%; : .center }

    On peut faire l'analogie avec des interrupteurs en série.

!!! warning "Remarque"

    On peut écrire toutes les tables de vérités en remplaçant les 0 par des F (pour Faux), et les 1 par des V (pour Vrai)


## III. Exemples d’expressions booléennes :

### 1. non (a et b)

???+ question "non (a et b)"

	Démontrer en utilisant des tables de vérité que les expressions booléennes suivantes sont équivalentes :

	* not (a and b)
	* not a or not b

	Pour cela recopier et remplir les tables de vérités suivantes :

	|a|b| a et b| non (a et b)|
    |:--:|:--:|:--:|:--:|
    |...|...|...|...|
    |...|...|...|...|
    |...|...|...|...|
    |...|...|...|...|

    |a|b| non a| non b|(non a) or (nonb)|
    |:--:|:--:|:--:|:--:|:--:|
    |...|...|...|...|...|
    |...|...|...|...|...|
    |...|...|...|...|...|
    |...|...|...|...|...|

    Les dernières colonnes de ces deux tableaux sont $\hspace{7em}$, ce qui prouve ... 

### 2. non (a ou b)

???+ question "non (a ou b)"

	Démontrer en utilisant des tables de vérité que les expressions booléennes suivantes sont équivalentes :

	* not (a or b)
	* not a and not b

	Pour cela recopier et remplir les tables de vérités suivantes :

	|a|b| a ou b| non (a ou b)|
    |:--:|:--:|:--:|:--:|
    |...|...|...|...|
    |...|...|...|...|
    |...|...|...|...|
    |...|...|...|...|

    |a|b| non a| non b|(non a) and (nonb)|
    |:--:|:--:|:--:|:--:|:--:|
    |...|...|...|...|...|
    |...|...|...|...|...|
    |...|...|...|...|...|
    |...|...|...|...|...|


    Les dernières colonnes de ces deux tableaux sont $\hspace{7em}$, ce qui prouve ...


!!! info "Remarque : ces deux propriétés sont connues sous le nom de lois de De Morgan."

## IV. Un opérateur supplémentaire : xor

???+ question "Fromage ou dessert ?"

	Lorsqu’au restaurant on vous demande « fromage ou dessert ? », quels sont les possibilités auxquelles vous avez le droit ?

	On peut choisir ...

	!!! info "Il s’agit du « ou » exclusif, noté **xor**."

!!! example "Exemple du jardinier"

    Un jardinier doit élaguer tous les arbres qui mesurent plus de 10 mètres ou qui ont plus de 10 ans.
	Peut-il tailler des arbres qui mesurent plus de 10 mètres et ont plus de 10 ans ?

	Il ...

	!!! info "Il s’agit du « ou » inclusif, noté **or**."

???+ question "Table de vérité de l'opérateur xor"

	Recopier et compléter la table de vérité

    |x|y| x xor  y| 
    |:--:|:--:|:--:|
    |...|...|...|
    |...|...|...|
    |...|...|...|
    |...|...|...|

## V. Variables booléennes et Python

### 1. Les variables booléennes

!!! example "Tester en console"

    Que s'affiche-t-il dans la console ?

    ```pycon
    >>> 3 == 5

    >>> 3 == "3"

    >>> 3 < 5

    >>> 3 != 5

    ```

!!! example "Des variables booléennes"

	Souvent les variables booléennes sont le résultat de tests :

    Que s'affiche-t-il dans la console ?

    ```pycon
    >>> a = -3**2 == -9

    >>> type(a)

    >>> a

    >>> a = (-3)**2 == -9

    >>> a
	```


### 2. Python et les opérateurs

!!! info "Les booléens en Python"

	En Python, les booléens peuvent prendre les valeurs `True` et `False`.

	Les opérations booléennes de bases sont `and`, `or` et `not`.

!!! example "Tester en console"

    Que s'affiche-t-il dans la console ?

    ```pycon
    >>> True and False

    >>> True or False

    >>> not True

    ```


!!! example "Les priorités"

	Comme pour les opérations mathématiques, il y a des priorités sur les opérations booléennes.
    
    Que s'affiche-t-il dans la console ?

    ```pycon
    >>> True or True and False

    >>> (True or True) and False

    >>> True or (True and False)

    ```

!!! info "Les priorités"

	Le ... est prioritaire sur le ... . De même, ... est prioritaire sur les autres opérations.

	Que s'affiche-t-il dans la console ?

    ```pycon
    >>> not False and False

    >>> not (False and False)

    >>> (not False) and False

    ```


!!! danger "Attention"

    Pour éviter toute confusion, il est vivement recommandé d’utiliser des parenthèses.

### 3. Exemples 

???+ question "Question 1"

	Expliquer pourquoi l’expression 3 == 3 or x == y est vraie pour toute valeur de x et de y.

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$



???+ question "Question 2"

	Expliquer pourquoi l’expression 1 == 2 and x == y est fausse pour toute valeur de x et de y.

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

## VI. Caractère séquentiel de certains opérateurs booléens.

!!! info "Python paresseux"

	Lorsque Python évalue une expression booléenne, il le fait de façon paresseuse. C’est à dire que si la partie gauche d’un or est vraie, il n’évalue pas la partie droite. De même si la partie gauche d’un and est fausse, la partie droite n’est pas évaluée.

!!! example "Tester les évaluations paresseuses"

	Que s'affiche-t-il dans la console ?

    ```pycon
    >>> x = 0

    >>> x == 0 or 0 < 1/x < 1

    >>> x !=0 and 0 < 1/x < 1

    ```



    !!! failure "Diviser par 0 ?"

    	Si la division 1/x était évaluée, il y aurait une erreur, puisque l’on ne peut pas diviser par 0.
  

    !!! info "Python paresseux"

    	Dans les deux cas, l’évaluation n’est pas faite puisque le résultat de l’expression a déjà pu être déterminé grâce à la partie gauche.

## VII. Exercices

???+ question "Exercice 1"

	Déterminer la table de vérité de : **a ou (non b)**

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$


???+ question "Exercice 2"

	Déterminer la table de vérité de : **(non a) et b**

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

???+ question "Exercice 3 à utiliser pour les exercices suivants"

	Faire un arbre de toutes les possibilités avec trois propositions a, b et c comme nous l’avons fait en cours pour deux propositions.

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$



???+ question "Exercice 4"

	Déterminer la table de vérité de : **(a ou b) et c**

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$


???+ question "Exercice 5"

	Déterminer la table de vérité de : **(a et b) ou c**

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$
	

???+ question "Exercice 6"

	Démontrer que a xor b est équivalent à : (a ou b) et (non (a et b))   

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$

	$\hspace{5em}$





	
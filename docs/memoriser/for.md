---
author: Mireille Coilhac
title: Boucle for - Bilan
---

!!! danger "Attention à ne pas oublier les deux points et l'indentation."

!!! abstract "À retenir ❤"
    
    * La boucle `for ... in ...` s'utilise lorsque :
        * on veut parcourir un à un les éléments d'un objet itérable (une chaîne de caractère, une liste, un tuple, un dictionnaire...)
        * on veut répéter une action un nombre de fois connu à l'avance. On parle de boucle bornée.

    * Les instructions répétées peuvent **_mais ce n'est pas obligatoire_** faire appel à la variable de boucle, mais il ne faut pas que ces instructions la modifient.

    * Ne pas oublier les **`:`** et l'indentation !

    * `range(n)` génère une séquence de `n` nombres entiers: on s'en servira dès qu'on aura besoin de répéter `n` fois des instructions.


!!! abstract "`for i in range(n)`"

	* `i` prend toutes les valeurs entières de `[0; n[`
	* `i` prend donc `n` valeurs différentes de 0 à `n` - 1.

	!!! example "Exemple"

    	```python
    	for i in range(3):
    		print(i)
    	```
    	
    	Ce script s'exécute ainsi en console : 
    	```pycon
    	0
		1
		2
		>>> 
		```

!!! abstract "`for i in range(a, b)`"

	* `i` prend toutes les valeurs entières de `[a; b[`.
	* `i` prend donc `b` - `a` valeurs différentes de `a` à `b` - 1.

	!!! example "Exemple"

    	```python
    	for i in range(3, 7):
    		print(i)
    	```
    	
    	Ce script s'exécute ainsi en console : 
    	```pycon
    	3
		4
		5
		6
		>>> 
		```

!!! abstract "`for i in range(a, b, pas)`"

	* `i` prend les valeurs entières de `[a; b[` en commençant à `a`, puis tous les `pas`.

	!!! example "Exemple"

    	```python
    	for i in range(3, 9, 2):
    		print(i)
    	```
    	
    	Ce script s'exécute ainsi en console : 
    	```pycon
    	3
		5
		7
		>>> 
		```



---
author: Mireille Coilhac
title: Opérateurs - Bilan
---

## I. Les opérateurs

!!! info "Opérateur"

	Lorsque l'on fait une opération, on applique un **opérateur** sur un ou plusieurs **opérandes**

!!! example "`2 + 3`"

	`2 + 3` : l'opérateur est **`+`**, signe de l'addition. Il y a 2 opérandes qui sont **2** et **3**.

!!! info "Opérandes"

	Lorsqu' il y a deux opérandes, on parle d’opérateurs binaires.
 
 	Les opérandes ne sont pas toujours des nombres, ce peut être des `int`, des `float` des `str` ou des `bool`, mais on fait aussi des opérations plus complexes sur d'autres objets .

## II. Opérateurs avec les nombres


|Opérations|	Symboles|	Exemples|
|:--|:--:|:--|
|addition|	`+`	|`2 + 5` donne 7|
|soustraction|	`-`	|`8 - 2` donne 6|
|multiplication	|`*`	|`6 * 7` donne 42|
|exponentiation (puissance)|	`**`|	`5 ** 3` donne 125|
|division	|`/`	|`7 / 2` donne 3.5|
|reste de division entière|	`%`|	`7 % 3` donne 1|
|quotient de division entière|	`//`|	`7 // 3` donne 2|

!!! info "modulo (`%`) et division entière (`//`)"

	* L'opérateur modulo donne le reste de la division euclidienne.   
	$7=2 \times 3 + 1$ donc `7 % 3` donne 1

	* L'opérateur division entière donne le quotient.  
	$7=2 \times 3 + 1$ donc `7 // 3` donne 2


!!! example "Exemples"

	17 divisé par 8 : "il y va deux fois" et il reste 1. ($17 = 2 \times 8 +1$)  
	👉 `17 % 8` vaut 1  
	👉 `17 // 8` vaut 2

	Alors que : `17 / 8` vaut 2.125

!!! info "Exponentiation"

	L'exponentiation (`**`) est aussi appelée puissance.

	L'opérateur `**` se lit puissance (c'est l'exponentiation) 

	Exemple :  `3**2` vaut 9

## III. Opérateurs avec les chaînes de caractères

|opérande1|	opérateur|	opérande2	|nom de l'opération|	exemple|	résultat|
|:--:|:--:|:--:|:--|:--|:--|
|str	|+	|str|	concaténation	|`"bon" + "jour"`|	`"bonjour"`|
|str|	`*`|	int|	répétition|	`"Aie" * 3`	|`"AieAieAie"`|
|int|	`*`|	str	|répétition|	`3 * "Aie"`|	`"AieAieAie"`|
|str|	in	|str|	est dans|	`"a" in "blabla"`|	`True`|
|str|	in	|str|	est dans|	`"e" in "blabla"`	|`False`|


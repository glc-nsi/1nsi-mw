import folium

coord = [..., ..., ..., 'N', ..., ..., ..., 'W']


lat = coord[0] + coord[1] / 60 + coord[2] / 3600
if coord[3] == "S":
    lat = - lat
long = coord[4] + coord[5] / 60 + coord[6] / 3600
if coord[7] == "W":
    long = - long
carte = folium.Map(location = [lat, long], zoom_start = 17)
folium.Marker([lat, long], popup = "lieu recherché").add_to(carte)
carte

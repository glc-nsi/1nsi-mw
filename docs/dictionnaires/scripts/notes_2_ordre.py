def bulletin(les_notes) -> list:
    liste_notes = []
    for devoir in les_notes:
        liste_notes.append((devoir, les_notes[devoir]))
    return liste_notes

print({"test_1": 14, "test_2": 16, "test_3": 18} == {"test_3": 18, "test_2": 16, "test_1": 14})
print(bulletin({"test_1": 14, "test_2": 16, "test_3": 18}))
print(bulletin({"test_3": 18, "test_2": 16, "test_1": 14}))

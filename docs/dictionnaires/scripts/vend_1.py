def vend(ferme):
    ...

ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 1, "cheval": 4}
vend(ferme_gaston)

# Tests
assert ferme_gaston == {"lapin": 4, "vache": 6, "cochon": 0, "cheval": 3}

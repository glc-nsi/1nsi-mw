from collections import namedtuple  # Pour utiliser des tuples nommés

# on peut séparer les noms des clés par des espaces ou des virgules
Personne = namedtuple('personne','nom prenom annee_naissance mail ville' )

alice = Personne("Martin", "Alice", 2003, "alice@mon_mail.fr", "Melun")
bob = Personne("Dupond", "Bob", 2004, "bob@mon_mail.fr", "Le Mée")
gaston = Personne("Durand", "Gaston", 2000, "gaston@mon_mail.fr", "Savigny")
print(alice.nom)
print(bob.mail)
print(gaston.ville)

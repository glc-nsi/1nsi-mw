def init_dict() :
    """
    In : rien
    Out: in dictionnaire décrivant une ferme avec 3 cochons et 7 vaches
    """
    return {"cochon": 3, "vache": 7}

def achat_vente(quantite, animal, ferme) :
    """
    In :
      quantité : int : quantité à vendre/acheter (négatif si vente)
      animal : str : animal a vendre/acheter
      ferme : dict : dictionnaire décrivant la ferme
    Out:

       Si quantité est négatif : opération de vente
           - vérifier que le stock de l'animal est suffisant
           si oui :
               - modifier le stock
               - écrire en console "Vente effectuée"
               - si le stock pour cet animal est 0, supprimer la clef dans le dict
               - renvoyer 0
           si non :
               - ne rien modifier
               - écrire en console "Vente effectuée"
               - renvoyer 1

        Si quantité est positif : opération d'achat
           - vérifier si l'animal est déjà dans la ferme
           si oui :
               - modifier le stock,
               - écrire en console "Achat effectué"
               - renvoyer 0
           si non :
               - créer une clef, avec la bonne valeur
               - écrire en console "Achat effectué"
               - renvoyer 0
    """

    if quantite < 0 :
        # C'est une vente
        if ferme[animal] >= - quantite :
            ferme[animal] += quantite
            print("Vente effectuée !")
            if ferme[animal] == 0 :
                del ferme[animal]
            return 0
        else :
            print("Vente annulée !")
            return 1
    else :
        # C'est un achat
        if animal in ferme :
            ferme[animal] += quantite
            print("Achat effectué !")
        else :
            ferme[animal] = quantite
            print("Achat effectué !")
        return 0

def nb_especes(ferme) :
    """
    In : le dictionnaire de la ferme
    Out : le nombre d'espèces

    Exemple : si ferme = {"vache": 3, "cochon": 4, "poulet": 5}
    la fonction devra renvoyer 3
    """
    return len(ferme)

def especes(ferme) :
    """
    In : le dictionnaire de la ferme
    Out: str : la fonction renvoie une chaine avec les animaux, séparés par " / "

    Exemple : si ferme = {"vache": 3, "cochon": 4, "poulet": 5}
    la fonction devra renvoyer "vache / cochon / poulet"
    """
    return " / ".join(ferme.keys())

def nb_total_animaux(ferme) :
    """
    In : le dictionnaire de la ferme
    Out: int : la fonction renvoie le nombre total d'animaux

    Exemple : si ferme = {"vache": 3, "cochon": 4, "poulet": 5}
    la fonction devra renvoyer 12
    """
    return sum(ferme.values())
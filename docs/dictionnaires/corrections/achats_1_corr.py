def facture(achats, prix) -> float:
    a_payer = 0
    for article in achats:
        a_payer = a_payer + achats[article] * prix[article]
    return a_payer


mes_achats = {"farine": 2, "beurre": 1, "sucre": 2}
mes_prix = {"farine": 1.2, "sucre": 2.5, "lait": 1.5, "beurre": 2.3}
assert facture(mes_achats, mes_prix) == 9.7

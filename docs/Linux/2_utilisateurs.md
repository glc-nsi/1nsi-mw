---
author: Mireille Coilhac
title: Utilisateurs
---

## I. Introduction

!!! info "Les comptes utilisateurs"

    Un système d’exploitation de type Unix est un système d’exploitation multi-tâches et multi-utilisateurs. Cela signifie que sur une même machine plusieurs personnes peuvent travailler simultanément.

	Le système doit donc pouvoir gérer plusieurs utilisateurs en même temps en assurant à la fois le partage des ressources (espace disque, utilisation de la mémoire, périphériques, etc), la confidentialité des données de chaque utilisateur et bien sûr l’intégrité de l’arborescence des répertoires et des fichiers.

	Puisque plusieurs personnes peuvent être connectées en même temps le système doit pouvoir identifier clairement chacun des utilisateurs ainsi que les ressources auxquelles ils ont accès et plus généralement qui a le droit de faire quoi.

	Ainsi, chaque personne autorisée à utiliser un système de type Unix se voit attribuer un **compte utilisateur**. Il existe un ensemble de règles qui régissent ce qu’un utilisateur a le droit de faire.

## II. Les différents utilisateurs

!!! info "Le super-utilisateur et les autres"

	Il y a deux types d’utilisateurs, comme le montre la figure ci-dessous :

	![groupes](images/figure_groupes.png){ width=60% }

	* Un super-utilisateur qui a le droit de faire tout ce qu’il veut sur le système, absolument tout : créer des utilisateurs, leur accorder des droits, supprimer des utilisateurs, avoir accès à leurs données, modifier le système. Ce super-utilisateur s’appelle root. Cet utilisateur c’est l’administrateur du système.
	* Et les autres utilisateurs. Ceux-là n’ont qu’une possibilité d’action limitée et surtout pas la possibilité de modifier le système. Ces utilisateurs peuvent être répartis dans différents groupes

!!! info "se connecter à un compte utilisateur"

	Pour être identifié sur un système de type Unix, il faut posséder un compte utilisateur, créé par le super-utilisateur, et caractérisé par un identifiant de compte et un mot de passe.

	👉 Pour se connecter sur le système par une console et ouvrir une session de travail, il faut entrer son identifiant à l’invite login puis son mot de passe à l’invite passwd .

	🌵 Afin de rester confidentiel, la saisie du mot de passe se fait en aveugle.


!!! info "utilisateur id : uid"

	👉 En plus de son identifiant de compte, chaque utilisateur est identifié par un numéro unique **uid** (user identifier) et appartient à un groupe principal gid (group identifier) et éventuellement à des groupes secondaires d’utilisateurs.

	👉 Le groupe principal est utilisé par le système en relation avec les droits d’accès aux fichiers. Chaque utilisateur doit appartenir à un groupe principal.

	👉 Les groupes secondaires sont les autres groupes auxquels un utilisateur appartient. Un utilisateur peut au maximum appartenir à 1024 groupes secondaires.  

	👉 Pour connaître son uid et les groupes auxquels on appartient, on peut utiliser la commande `id` (**id**entity).


!!! example "Exemple"

	![ID](images/Capture_id.jpg){ width=80% }

	Dans cet exemple, l’identifiant de compte de l’utilisateur porte le nom john, son numéro d’identification est 1000, son groupe principal est le groupe python identifié par le numéro 1000. On voit aussi qu’il appartient à plusieurs groupes secondaires de gid 4, 24, 27 et 118


!!! info "Le fichier /etc/passwd"

	C’est dans ce fichier que se trouvent les informations de connexion de tous les utilisateurs du système. Il s’agit d’un fichier texte où chaque ligne correspond à un utilisateur. Cette ligne est composée de sept champs séparés par le caractère ": " comme le montre la figure suivante.

	![etc](images/capture_etc.png){ width=80% }

	Les informations présentes sur cette ligne sont, dans l’ordre :

	* l’identifiant de compte que communément on appelle aussi le nom de l’utilisateur
	* le mot de passe crypté pour cet utilisateur (sur certain systèmes ce champ ne contient pas le mot de passe crypté mais le caractère ’x’ qui indique que le mot de passe est stocké ailleurs dans un fichier /etc/shadow visible uniquement par le super-utilisateur, si ce champ est vide cela signifie que l’utilisateur n’a pas de mot de passe) ;
	* l’**uid** de l’utilisateur ;
	* le **gid** de son groupe principal ;
	* un champ, appelé **gcos** , rempli librement par l’utilisateur et qui contient la description de l’utilisateur ;
	* le répertoire personnel de l’utilisateur (home directory). Habituellement le nom de ce répertoire est `/home/login` , avec login l’identifiant de compte ;
	* le shell de connexion de l’utilisateur, c’est-à-dire le shell utilisé par l’utilisateur lorsqu’il ouvre une session. (Il existe d’autres types de shell que celui du Bash.)


!!! info "Le fichier /etc/shadow"

	Ce fichier, s’il existe, contient les mots de passe cryptés de tous les utilisateurs, il n’est visible que par le super-utilisateur. Chaque ligne de ce fichier texte est associée à un utilisateur et contient un certain nombre de champs d’information que le super-utilisateur peut renseigner pour la gestion des comptes : la date à laquelle le mot de passe a été modifié pour la dernière fois, le nombre de jours avant expiration du mot de passe, le nombre de jours restant avant le prochain changement obligatoire du mot de passe, le nombre de jours pendant lesquels le compte reste actif après expiration du mot de passe, la date de désactivation du compte.

	Remarque : le système installé sur la Weblinux qui accompagne le TP suivant ce cours n’a pas de fichier `/etc/ shadow` , le mot de passe crypté est donc stocké dans le fichier `/etc/passwd .`


!!! info "Le fichier /etc/group"

	C’est grâce à ce fichier texte qu’un système de type Unix peut gérer des groupes d’utilisateurs. Chaque ligne correspond à un groupe. Une ligne est composée de champs séparés par : .

	* le nom du groupe ;
	* le mot de passe du groupe (ce champ est rarement utilisé, par contre s’il est renseigné un utilisateur du groupe qui veut accéder à une ressource appartenant au groupe devra saisir ce mot de passe) ;
	* le numéro unique d’identification du groupe ;
	* la liste des utilisateurs qui appartiennent au groupe.


## III. Gestion des utilisateurs et des groupes

!!! info "Environnement de travail"

	Les systèmes de type "UNIX" sont des systèmes multi-utilisateurs, plusieurs utilisateurs peuvent donc partager un même ordinateur, chaque utilisateur possédant un environnement de travail qui lui est propre.


!!! info "Les groupes d'utilisateurs"

	Chaque utilisateur possède certains droits lui permettant d'effectuer certaines opérations et pas d'autres. Le système d'exploitation permet de gérer ces droits très finement. Un utilisateur un peu particulier est autorisé à modifier tous les droits : ce super utilisateur est appelé administrateur ou plus communément, root. L'administrateur pourra donc attribuer ou retirer des droits aux autres utilisateurs.

	Au lieu de gérer les utilisateurs un par un, il est possible de créer des groupes d'utilisateurs. L'administrateur attribue des droits à un groupe au lieu d'attribuer des droits particuliers à chaque utilisateur. Comme nous venons de le voir, chaque utilisateur possède des droits qui lui ont été octroyés par le "super utilisateur".


!!! info "r w x"

	Nous nous intéresserons ici uniquement aux droits liés aux fichiers, mais vous devez savoir qu'il existe d'autres droits liés aux autres éléments du système d'exploitation ((imprimante, installation de logiciels...). Les fichiers et les répertoires possèdent 3 types de droits :

	* les droits en lecture (symbolisés par la lettre **r**) : est-il possible de lire le contenu de ce fichier ?
	* les droits en écriture (symbolisés par la lettre **w**) : est-il possible de modifier le contenu de ce fichier ?
	* les droits en exécution (symbolisés par la lettre **x**) : est-il possible d'exécuter le contenu de ce fichier ? (quand le fichier du code est exécutable)


!!! info "u g o"

	Il existe 3 types d'utilisateurs pour un fichier ou un répertoire :

	* le propriétaire du fichier (par défaut c'est la personne qui a créé le fichier), il est symbolisé par la lettre **u**
	* un fichier est associé à un groupe, tous les utilisateurs appartenant à ce groupe possèdent des droits particuliers sur ce fichier.  
	Le groupe est symbolisé par la lettre **g**
	* tous les autres utilisateurs (ceux qui ne sont pas le propriétaire du fichier et qui n'appartiennent pas au groupe associé au fichier).  
	Ces utilisateurs sont symbolisés par la lettre **o** (other).


!!! info "Lecture des droits"

	Il est possible d'utiliser la commande `ls` avec l'option `ls -l` afin d'avoir des informations supplémentaires.

	Nous sommes positionnés dans le répertoire Arbres et nous avons exécuté la commande `ls -l`

	![permissions 1](images/permissions1.png){ width=80% }

	Lisons cette ligne de gauche à droite :

	**-rw-r--r--**

	* le premier symbole **-** signifie que l'on a affaire à un fichier, dans le cas d'un répertoire,nous aurions un **d** (directory)
	* les 3 symboles suivants **rw-** donnent les droits du **propriétaire du fichier** : lecture autorisée (r), écriture autorisée (w), exécution interdite (- à la place de x)
	* Les 3 symboles suivants **r--** donnent les droits du **groupe lié au fichier** : lecture autorisée (r), écriture interdite (- à la place de w), exécution interdite (- à laplace de x)
	* Les 3 symboles suivants **r--** donnent les droits des **autres utilisateurs** : lecture autorisée (r), écriture interdite (- à la place de w), exécution interdite (- à la place de x)

	![droits 2](images/droits_2.png){ width=70% }

	Un peu plus loins sur la ligne :

	* Le caractère suivant "1" donne le nombre de liens (nous n'étudierons pas cette notion ici)
	* Le premier "user" représente le nom du propriétaire du fichier
	* Le second "user" représente le nom du groupe lié au fichier
	* Le "0" représente la taille du fichier en octet (ici notre fichier est vide)
	* Feb 20 14:59 donne la date et l'heure de la dernière modification du fichier
	* bouleau.txt est le nom du fichier


!!! info "La commande chmod"

	Il est important de ne pas perdre de vu que l'utilisateur root a la possibilité de modifier les droits de tous les utilisateurs. Le propriétaire d'un fichier peut modifier les permissions d'un fichier ou d'un répertoire à l'aide de la commande chmod.

	Pour utiliser cette commande, il est nécessaire de connaitre certains symboles :

	* Les symboles liés aux utilisateurs : **u** correspond au propriétaire, **g** correspond au groupe lié au fichier (ou au répertoire), **o** correspond aux autres (other) utilisateurs et **a** (all) correspond à "tout le monde" (permet de modifier u, g et o en même temps)
	* Les symboles liés à l'ajout ou la suppression des permissions : **+** on ajoute une permission, **-** on supprime une permission, **=** les permissions sont réinitialisées (permissions par défaut)
	* Les symboles liés aux permissions : **r** : lecture, **w** : écriture, **x** : exécution.

	👉 La commande "chmod" a cette forme : **chmod [u g o a] [+ - =] [r w x] nom_du_fichier**


!!! example "Exemples"

	* Un premier exemple : `chmod o+w toto.txt` attribuera la permission "écriture" pour le fichier `toto.txt` aux autres utilisateurs.

	* Un second exemple : l'utilisateur user travaille dans son répertoire home (/home/user). Il crée avec `touch` un fichier `exemple`, et liste le contenu de son répertoire `home`.

	![touch](images/touch.jpg){ width=70% }

	Il change ensuite les permissions, pour rendre le fichier exécutable pour lui même (mais pas pour les autres utilisateurs du groupe ou hors groupe) et liste à nouveau le contenu du répertoire courant :

	![chmod](images/chmod.jpg){ width=70% }


???+ question

    On a ceci :

    ![fleurs 1](images/fleurs_1.png){ width=70% }

    Comment modifier les permissions associées au fichier pour que tout le monde ait la permission lecture et écriture ? On doit obtenir les droits indiqués ci-dessous :

    ![fleurs 2](images/fleurs_2.png){ width=70% }

    ??? success "Solution"

        `chmod a+rw hortensia.txt`

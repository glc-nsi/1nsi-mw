---
author: Mathias Weislinger
title: 🏡 Accueil du cours de première NSI ( groupe de M.Weislinger )

---


Bienvenue sur le site de NSI de niveau première du **Lycée Guillaume Le Conquérant**


🛠️ Ce site est en **construction ...**


# Outils essentiels 

## L'IDE du site

!!! question "Comment utiliser l'environnemnt de développement intégré (IDE) utilisé sur ce site 😊"
    De nombreux exercices ```Python``` sont proposés et sont réalisables directement sur ce site. Un ensemble de **"jeu de tests"** est prévu pour s'auto évaluer.  

    ??? info "Voir un exemple de l'IDE"
        L'éditeur comporte trois zones (dépliez le bloc ci-dessous pour voir un exemple):

        * une zone de saisie (partie supérieure);

        * un terminal (partie centrale, vide initialement);

        * des boutons (en bas).

        !!! tip "L'éditeur"
 
            {{ IDE('Python/scripts/somme_exemple.py', ID=1,MAX=3)}}
        
<!--

!!! warning "L'IDE est doté **de plusieurs boutons :**"
    <div style="text-align:center;">
    <button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img src="./assets/images/icons8-play-64.png" alt="Valider"></button>
    <button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img src="./assets/images/icons8-check-64.png" alt="Valider"></button>
    <button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img src="./assets/images/icons8-download-64.png" alt="Valider"></button>
    <button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img src="./assets/images/icons8-upload-64.png" alt="Valider"></button>
    <button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img src="./assets/images/icons8-save-64.png" alt="Valider"></button>
    <button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img src="./assets/images/icons8-restart-64.png" alt="Valider"></button>
    </div>

-->


??? abstract  "**Voici un descriptif détaillé du fonctionnement de l'IDE.**"

    === "Tests publics"
        Vous devez compléter le code dans la zone de saisie. Les assertions constituent les **tests publics**. Il reprennent le plus souvent les exemples de l'énoncé.

        Une ligne du type `#!py assert somme(10, 32) == 42` vérifie que la fonction `somme` renvoie bien `#!py 42` lorsqu'on lui propose `#!py 10` et `#!py 32` comme arguments.

        Vous pouvez vérifier que votre fonction passe ces tests publics en cliquant sur le bouton <span class="py_mk_ide"><button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img src="./assets/images/icons8-play-64.png" alt="Exécuter les tests"></button><span>.

    === "Tests privés"
        Une fois les tests publics passés, vous pouvez passer les **tests privés** en cliquant sur le bouton **Valider** <span class="py_mk_ide"><button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img src="./assets/images/icons8-check-64.png" alt="Valider"></button><span>.

        Ceux-ci sont plus nombreux et, comme leur nom l'indique, ne vous sont pas connus. Seul leur résultat vous est indiqué avec, parfois, un commentaire sur la donnée ayant mis en défaut votre code.

        Dans la plupart des exercices, un compteur permet de suivre vos essais. Ce compteur est décrémenté à chaque fois que vous cliquez sur le bouton **Valider** <span class="py_mk_ide"><button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img src="./assets/images/icons8-check-64.png" alt="Valider"></button><span> effectuant les tests privés. Lorsqu’il atteint 0, la solution de l’exercice vous est proposée.

    === "Autres boutons"
        Il est aussi possible de :

        * <span class="py_mk_ide"><button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img src="./assets/images/icons8-download-64.png" alt="Télécharger"></button><span> : télécharger le contenu de l'éditeur si vous souhaitez le conserver ou travailler en local ;

        * <span class="py_mk_ide"><button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img src="./assets/images/icons8-upload-64.png" alt="Téléverser"></button><span> : téléverser un fichier Python dans l'éditeur afin de rapatrier votre travail local ;

        * <span class="py_mk_ide"><button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img src="./assets/images/icons8-restart-64.png" alt="Réinitialiser l'IDE"></button><span> : recharger l'éditeur dans son état initial ;

        * <span class="py_mk_ide"><button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img src="./assets/images/icons8-save-64.png" alt="Sauvegarder"></button><span> : sauvegarder le contenu de l'éditeur dans la mémoire de votre navigateur ;


## [Autres outils](outils/outils.md)

## Crédits

Pour la présentation des IDE : [CodEX](https://codex.forge.apps.education.fr/)


_mise à jour le 24/10/2024_
